import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private http: HttpClient) { }

  getFlightDetails(flight: any): Observable<HttpResponse<any>> {
    return this.http.get<any>('api/flight/origin/detination/load?origin=' + flight.origin + '/destination=' + flight.destination + '/date=' + flight.date, { observe: 'response' });
  }
}
