import { Component } from '@angular/core';
import { FlightService } from './flight.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  flight: any;

  constructor(private flightService: FlightService) {
    this.flight = {};
  }

  getFlightDetails() {
    debugger
    this.flight;
    this.flightService.getFlightDetails(this.flight).subscribe(response => {

      console.log(response);
    })
  }
}
