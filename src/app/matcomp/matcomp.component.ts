import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: "app-matcomp",
  templateUrl: "./matcomp.component.html",
  styleUrls: ["./matcomp.component.scss"],
})
export class MatcompComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor() {}

  ngOnInit() {}
   leftNav: any = [
    { position: 1, name: "Michele", weight: 'CP', symbol: "H" },
    { position: 2, name: "Hennry", weight: 'CK', symbol: "He" },
    { position: 3, name: "Drawe", weight: 'NZ', symbol: "Li" },
    { position: 4, name: "Hyden", weight: 'LU', symbol: "Be" },
    { position: 5, name: "Boron", weight: 'LI', symbol: "B" },
    { position: 6, name: "Carbon", weight: 'KM', symbol: "C" },
    { position: 7, name: "Andrew", weight: 'PK', symbol: "N" },
    { position: 8, name: "Trump", weight: 'LS', symbol: "O" },
    { position: 9, name: "Torzon", weight: 'RP', symbol: "F" },
    { position: 10, name: "Neon", weight: 'ST', symbol: "Ne" },
  ];

  arrValue: any = [
    { position: 1, name: "Hydrogen", weight: 1.0079, symbol: "H" },
    { position: 2, name: "Helium", weight: 4.0026, symbol: "He" },
    { position: 3, name: "Lithium", weight: 6.941, symbol: "Li" },
    { position: 4, name: "Beryllium", weight: 9.0122, symbol: "Be" },
    { position: 5, name: "Boron", weight: 10.811, symbol: "B" },
    { position: 6, name: "Carbon", weight: 12.0107, symbol: "C" },
    { position: 7, name: "Nitrogen", weight: 14.0067, symbol: "N" },
    { position: 8, name: "Oxygen", weight: 15.9994, symbol: "O" },
    { position: 9, name: "Fluorine", weight: 18.9984, symbol: "F" },
    { position: 10, name: "Neon", weight: 20.1797, symbol: "Ne" },
  ];

  
}


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];