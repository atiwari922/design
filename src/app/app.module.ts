import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TabsModule, ButtonsModule , BsDatepickerModule  } from 'ngx-bootstrap';
import { MatcompComponent } from './matcomp/matcomp.component';
import { routing } from './app.router';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule, MatGridListModule, MatDividerModule, MatTableModule} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    MatcompComponent
  ],
  imports: [
    BrowserModule,
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
    MatSidenavModule,
    MatGridListModule,
    MatDividerModule,
    MatTableModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
