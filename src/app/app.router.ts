import { RouterModule, Routes, CanActivate } from '@angular/router';
import { MatcompComponent } from './matcomp/matcomp.component';

const appRoutes: Routes = [
  {
    path: "home",
    component: MatcompComponent,
  },
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full",
  },
];

export const routing = RouterModule.forRoot(appRoutes, {
  useHash: false,
});
